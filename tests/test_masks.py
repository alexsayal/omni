# pylint: disable=unused-import,redefined-outer-name
import numpy as np
from omni import masks
from . import fake_epi_and_mask


def test_lda():
    test_data = np.random.rand(100, 10)
    test_labels = np.random.randint(0, 2, size=100)
    result = masks.lda(test_data, test_labels)
    assert test_data.shape == result.shape


def test_generate_noise_mask(fake_epi_and_mask):
    fake_data_img = fake_epi_and_mask[0]
    mask_img = fake_epi_and_mask[1]
    noise_mask, smooth_noise_mask = masks.generate_noise_mask(fake_data_img, mask_img, iterations=3)
    assert mask_img.shape == noise_mask.shape
    assert mask_img.shape == smooth_noise_mask.shape
