import os
import unittest
import nibabel as nib
import numpy as np
from omni import interface
from . import TESTDATA


class TestInterface(unittest.TestCase):
    def test_omniinterface(self):
        # create interface object
        omniinterface = interface.OmniInterface()
        self.assertIsInstance(omniinterface, interface.OmniInterface)

        # load test image
        img = nib.load(
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        )

        # this should do nothing...
        omniinterface.link = None

        # set image in interface
        omniinterface.set_img("test", img)

        # get image from interface
        img1 = omniinterface.get_img("test", clip=(
            np.min(img.get_fdata().ravel()), np.max(img.get_fdata().ravel())))
        img2 = omniinterface.get_img("test", img.affine)
        img3 = omniinterface.get_img("test", ref=img)

        # test equality
        self.assertTrue(np.all(np.isclose(img.get_fdata(), img1.get_fdata())))
        self.assertFalse(np.all(np.isclose(img.affine, img1.affine)))
        self.assertTrue(np.all(np.isclose(img.get_fdata(), img2.get_fdata())))
        self.assertTrue(np.all(np.isclose(img.affine, img2.affine)))
        self.assertTrue(np.all(np.isclose(img.get_fdata(), img3.get_fdata())))
        self.assertTrue(np.all(np.isclose(img.affine, img3.affine)))

        # test get_img exception
        with self.assertRaises(ValueError):
            omniinterface.get_img("test", img.affine, ref=img)
