import os
import unittest
import tempfile
import nibabel as nib
import numpy as np
from omni import io
from . import TESTDATA


class TestIO(unittest.TestCase):
    def test_load_target_source(self):
        target, source = io.load_target_source(
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"),
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        )
        self.assertIsInstance(target, nib.Nifti1Image)
        self.assertIsInstance(source, nib.Nifti1Image)

    def test_load_interaction_spec(self):
        model = io.load_interaction_spec(
            os.path.join(TESTDATA, "models", "test.model")
        )
        model2 = io.load_interaction_spec(
            'rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)')
        self.assertEqual(model, 'rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)')
        self.assertEqual(model2, 'rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)')

    def test_read_omni_affine(self):
        affine = io.read_omni_affine(
            os.path.join(TESTDATA, "affines", "test.affine")
        )
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        self.assertTrue(np.all(np.isclose(affine, test)))

    def test_read_afni_affine(self):
        affine = io.read_afni_affine(
            os.path.join(TESTDATA, "affines", "test.aff12.1D")
        )
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        self.assertTrue(np.all(np.isclose(affine, test)))

    def test_read_affine_file(self):
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        afni, afni_type = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test.aff12.1D")
        )
        fsl, fsl_type = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test.mat")
        )
        omni, omni_type = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test.affine")
        )
        self.assertEqual(afni_type, "afni")
        self.assertTrue(np.all(np.isclose(afni, test)))
        self.assertEqual(fsl_type, "fsl")
        self.assertTrue(np.all(np.isclose(fsl, test)))
        self.assertEqual(omni_type, "omni")
        self.assertTrue(np.all(np.isclose(omni, test)))

        with self.assertRaises(ValueError):
            io.read_affine_file("test.test")

    def test_write_omni_affine(self):
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        with tempfile.NamedTemporaryFile() as f:
            io.write_omni_affine(f.name, test)
            a1 = io.read_omni_affine(f.name + '.affine')
            a2 = io.read_omni_affine(
                os.path.join(TESTDATA, "affines", "test.affine")
            )
            self.assertTrue(np.all(np.isclose(a1, a2)))

    def test_write_afni_affine(self):
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        with tempfile.NamedTemporaryFile() as f:
            io.write_afni_affine(f.name, test)
            a1 = io.read_afni_affine(f.name + '.aff12.1D')
            a2 = io.read_afni_affine(
                os.path.join(TESTDATA, "affines", "test.aff12.1D")
            )
            self.assertTrue(np.all(np.isclose(a1, a2)))

    def test_write_fsl_affine(self):
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        with tempfile.NamedTemporaryFile() as f:
            io.write_fsl_affine(f.name, test)
            a1 = io.read_omni_affine(f.name + '.mat')
            a2 = io.read_omni_affine(
                os.path.join(TESTDATA, "affines", "test.mat")
            )
            self.assertTrue(np.all(np.isclose(a1, a2)))

    def test_write_affine_file(self):
        test = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        with tempfile.NamedTemporaryFile() as f:
            io.write_affine_file(f.name, test, "afni")
            io.write_affine_file(f.name, test, "fsl")
            io.write_affine_file(f.name, test, "omni")
            afni, _ = io.read_affine_file(
                os.path.join(TESTDATA, "affines", "test.aff12.1D")
            )
            fsl, _ = io.read_affine_file(
                os.path.join(TESTDATA, "affines", "test.mat")
            )
            omni, _ = io.read_affine_file(
                os.path.join(TESTDATA, "affines", "test.affine")
            )
            tafni = io.read_afni_affine(f.name + '.aff12.1D')
            tfsl = io.read_omni_affine(f.name + '.mat')
            tomni = io.read_omni_affine(f.name + '.affine')
            self.assertTrue(np.all(np.isclose(afni, tafni)))
            self.assertTrue(np.all(np.isclose(fsl, tfsl)))
            self.assertTrue(np.all(np.isclose(omni, tomni)))

            with self.assertRaises(ValueError):
                io.write_affine_file("test.test", omni, "test")

    def test_write_regress_params(self):
        array = np.zeros((50,))
        with tempfile.NamedTemporaryFile() as f:
            io.write_regress_params(f.name, array)
            self.assertTrue(
                os.path.exists(f.name + '.regress')
            )

    def test_write_rb_params(self):
        array = np.zeros((50, 6))
        with tempfile.NamedTemporaryFile() as f:
            io.write_rb_params(f.name, array)
            self.assertTrue(
                os.path.exists(f.name + '.params')
            )

    def test_convert_affine_file(self):
        afni_affine_file = os.path.join(TESTDATA, "affines", "test.aff12.1D")
        fsl_affine_file = os.path.join(TESTDATA, "affines", "test.mat")
        img = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        with tempfile.NamedTemporaryFile() as f:
            io.convert_affine_file(f.name, afni_affine_file, "omni")
            self.assertTrue(os.path.exists(f.name + ".affine"))
        with tempfile.NamedTemporaryFile() as f:
            io.convert_affine_file(f.name, fsl_affine_file, "omni", target=img, source=img)
            self.assertTrue(os.path.exists(f.name + ".affine"))
