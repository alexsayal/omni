import os
import unittest
import numpy as np
import nibabel as nib
from omni import io, affine
from . import TESTDATA


class TestAffine(unittest.TestCase):
    def test_generate_rigid_transform(self):
        rotation1 = affine.generate_rigid_transform(45, 0, 0, [0, 0, 0])
        rotation2 = np.array([
                [1, 0, 0, 0],
                [0, 1/np.sqrt(2), -1/np.sqrt(2), 0],
                [0, 1/np.sqrt(2), 1/np.sqrt(2), 0],
                [0, 0, 0, 1]
            ])
        self.assertTrue(np.all(np.isclose(rotation1, rotation2)))

    def test_striu2mat(self):
        # test general case
        mat = affine.striu2mat(np.ones([66]))
        self.assertTupleEqual(mat.shape, (12, 12))

        # test error case
        with self.assertRaises(ValueError):
            affine.striu2mat(np.ones([2]))

    def test_compose_rotation(self):
        angles = [np.pi/4, 0, 0]
        rotation1 = affine.compose_rotation(*angles)
        rotation2 = np.array([
            [1, 0, 0],
            [0, 1/np.sqrt(2), -1/np.sqrt(2)],
            [0, 1/np.sqrt(2), 1/np.sqrt(2)]
        ])
        self.assertTrue(np.all(np.isclose(rotation1, rotation2)))

    def test_decompose_rotation(self):
        rotation = np.array([
            [1, 0, 0],
            [0, 1/np.sqrt(2), -1/np.sqrt(2)],
            [0, 1/np.sqrt(2), 1/np.sqrt(2)]
        ])
        angles1 = np.array(affine.decompose_rotation(rotation))
        angles2 = np.array([np.pi/4, 0, 0])
        self.assertTrue(np.all(np.isclose(angles1, angles2)))

    def test_deoblique(self):
        pimg = nib.load(
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"))
        oimg = nib.load(
            os.path.join(TESTDATA, "imgs", "test_oblique.nii.gz"))
        doimg = affine.deoblique(oimg)
        self.assertTrue(np.all(np.isclose(pimg.affine, doimg.affine)))

    def test_convert_affine(self):
        omni_aff, _ = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test2.affine"))
        afni_aff, _ = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test2.aff12.1D"))
        fsl_aff, _ = io.read_affine_file(
            os.path.join(TESTDATA, "affines", "test2.mat"))
        img = nib.load(
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz"))

        # Test various conversions
        # omni-to-omni
        omni_omni_aff = affine.convert_affine(omni_aff, "omni", "omni")
        self.assertTrue(np.all(np.isclose(omni_aff, omni_omni_aff)))
        omni_omni_aff = affine.convert_affine(omni_aff, "omni", "omni", True)
        self.assertTrue(
            np.all(np.isclose(omni_aff, np.linalg.inv(omni_omni_aff))))

        # omni-to-afni
        omni_afni_aff = affine.convert_affine(omni_aff, "omni", "afni")
        self.assertTrue(np.all(np.isclose(afni_aff, omni_afni_aff)))
        omni_afni_aff = affine.convert_affine(omni_aff, "omni", "afni", True)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(afni_aff), omni_afni_aff)))

        # omni-to-fsl
        omni_fsl_aff = affine.convert_affine(
            omni_aff, "omni", "fsl", target=img, source=img)
        self.assertTrue(np.all(np.isclose(fsl_aff, omni_fsl_aff)))
        omni_fsl_aff = affine.convert_affine(
            omni_aff, "omni", "fsl", True, target=img, source=img)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(fsl_aff), omni_fsl_aff)))

        # afni-to-afni
        afni_afni_aff = affine.convert_affine(afni_aff, "afni", "afni")
        self.assertTrue(np.all(np.isclose(afni_aff, afni_afni_aff)))
        afni_afni_aff = affine.convert_affine(afni_aff, "afni", "afni", True)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(afni_aff), afni_afni_aff)))

        # afni-to-omni
        afni_omni_aff = affine.convert_affine(afni_aff, "afni", "omni")
        self.assertTrue(np.all(np.isclose(omni_aff, afni_omni_aff)))
        afni_omni_aff = affine.convert_affine(afni_aff, "afni", "omni", True)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(omni_aff), afni_omni_aff)))

        # afni-to-fsl
        afni_fsl_aff = affine.convert_affine(
            afni_aff, "afni", "fsl", target=img, source=img)
        self.assertTrue(np.all(np.isclose(fsl_aff, afni_fsl_aff)))
        afni_fsl_aff = affine.convert_affine(
            afni_aff, "afni", "fsl", True, target=img, source=img)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(fsl_aff), afni_fsl_aff)))

        # fsl-to-fsl
        fsl_fsl_aff = affine.convert_affine(
            fsl_aff, "fsl", "fsl", target=img, source=img)
        self.assertTrue(np.all(np.isclose(fsl_aff, fsl_fsl_aff)))
        fsl_fsl_aff = affine.convert_affine(
            fsl_aff, "fsl", "fsl", True, target=img, source=img)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(fsl_aff), fsl_fsl_aff)))

        # fsl-to-omni
        fsl_omni_aff = affine.convert_affine(
            fsl_aff, "fsl", "omni", target=img, source=img)
        self.assertTrue(np.all(np.isclose(omni_aff, fsl_omni_aff)))
        fsl_omni_aff = affine.convert_affine(
            fsl_aff, "fsl", "omni", True, target=img, source=img)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(omni_aff), fsl_omni_aff)))

        # fsl-to-afni
        fsl_afni_aff = affine.convert_affine(
            fsl_aff, "fsl", "afni", target=img, source=img)
        self.assertTrue(np.all(np.isclose(afni_aff, fsl_afni_aff)))
        fsl_afni_aff = affine.convert_affine(
            fsl_aff, "fsl", "afni", True, target=img, source=img)
        self.assertTrue(
            np.all(np.isclose(np.linalg.inv(afni_aff), fsl_afni_aff)))

        # test exception raising for fsl
        with self.assertRaises(ValueError):
            fsl_omni_aff = affine.convert_affine(fsl_aff, "fsl", "omni")
        with self.assertRaises(ValueError):
            fsl_omni_aff = affine.convert_affine(fsl_aff, "omni", "fsl")

        # test exception for unknown input type
        with self.assertRaises(ValueError):
            fsl_omni_aff = affine.convert_affine(fsl_aff, "test", "omni")
        with self.assertRaises(ValueError):
            fsl_omni_aff = affine.convert_affine(fsl_aff, "omni", "test")
