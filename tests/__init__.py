import os
from subprocess import PIPE, run, CalledProcessError
import pytest
import numpy as np
import nibabel as nib

# set numpy random seed for tests
np.random.seed(0)

# Get reference to test directory
TESTDIR = os.path.abspath(os.path.dirname(__file__))

# Get path to test data
TESTDATA = os.path.join(TESTDIR, "test_data")

# afni check
try:
    run(["3dAllineate"], stdout=PIPE, stderr=False, check=True)
    AFNICHECK = False
except CalledProcessError:
    AFNICHECK = True
afni_exists = pytest.mark.skipif(AFNICHECK, reason="AFNI required.")

# fsl check
try:
    run(["flirt", "-version"], stdout=PIPE, stderr=False, check=True)
    FSLCHECK = False
except CalledProcessError:
    FSLCHECK = True
fsl_exists = pytest.mark.skipif(FSLCHECK, reason="FSL required.")

# ants check
try:
    run(["antsRegistration", "--version"], stdout=PIPE, stderr=False, check=True)
    ANTSCHECK = False
except CalledProcessError:
    ANTSCHECK = True
ants_exists = pytest.mark.skipif(ANTSCHECK, reason="ANTs required.")


@pytest.fixture
def fake_epi_and_mask():
    """Create fake EPI and mask data"""
    test = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
    mask = os.path.join(TESTDATA, "imgs", "test_mask.nii.gz")
    test_img = nib.load(test)
    data = test_img.get_fdata()
    fake_data = np.moveaxis(np.tile(data, (10, 1, 1, 1)), 0, 3)
    fake_data += np.random.rand(*fake_data.shape)*10  # randomize data
    fake_data_img = nib.Nifti1Image(fake_data, test_img.affine)
    mask_img = nib.load(mask)
    return [fake_data_img, mask_img]


@pytest.fixture
def fake_epi():
    """Fake EPI data for testing."""
    test = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
    metadata = os.path.join(TESTDATA, "imgs", "test.json")
    test_img = nib.load(test)
    data = test_img.get_fdata()
    fake_data = np.moveaxis(np.tile(data, (10, 1, 1, 1)), 0, 3)
    fake_data += np.random.rand(*fake_data.shape)*10  # randomize data
    fake_data_img = nib.Nifti1Image(fake_data, test_img.affine)
    return [fake_data_img, metadata]


@pytest.fixture
def afni_affine():
    """Afni affine for testing."""
    test = os.path.join(TESTDATA, "affines", "test2.aff12.1D")
    return test


@pytest.fixture
def fsl_affine():
    """Fsl affine (+data) for testing."""
    test = os.path.join(TESTDATA, "affines", "test2.mat")
    test2 = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
    return [test, test2]


@pytest.fixture
def omni_affine():
    """Omni affine for testing."""
    test = os.path.join(TESTDATA, "affines", "test2.affine")
    return test


@pytest.fixture
def plumb_oblique_data():
    plumb = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
    oblique = os.path.join(TESTDATA, "imgs", "test_oblique.nii.gz")
    mask = os.path.join(TESTDATA, "imgs", "test_mask.nii.gz")
    return plumb, oblique, mask


@pytest.fixture
def test_intmat():
    return os.path.join(TESTDATA, "imgs", "test_intmat.h5")


@pytest.fixture
def test_bids():
    return os.path.join(TESTDATA, "imgs", "dummy_bids")
