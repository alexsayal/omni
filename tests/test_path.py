import unittest
import os
import tempfile
from omni import path


class TestPath(unittest.TestCase):
    def test_get_wrapped_callable(self):
        @path.create_output_path
        def test_function_with_decorator(output_path, test_arg1, test_arg2):
            return output_path, test_arg1, test_arg2
        test_function = path.get_wrapped_callable(test_function_with_decorator)
        self.assertTupleEqual(
            test_function.__code__.co_varnames,
            ("output_path", "test_arg1", "test_arg2"))

    def test_create_output_path(self):
        # This function should fail
        with self.assertRaises(ValueError):
            @path.create_output_path
            def fail_function(test):
                return test
            fail_function(None)

        # This function should pass
        @path.create_output_path
        def pass_function(output_path, test):
            return output_path, test

        # Create temp dir to teset output path
        with tempfile.TemporaryDirectory() as d:
            test_path = os.path.join(d, "test_path")
            pass_function(test_path, 1)
            self.assertTrue(os.path.exists(test_path))

    def test_use_abspaths(self):
        current_dir = os.getcwd()
        with tempfile.NamedTemporaryFile() as f:
            rel_f = os.path.relpath(f.name, current_dir)

            @path.use_abspaths
            def test_function(test, test2, test3="/test", test4=0):
                return test, test2, test3, test4
            abs_f1, num1, abs_f2, num2 = test_function(rel_f, 1, test3=rel_f, test4=2)
            self.assertEqual(abs_f1, f.name)
            self.assertEqual(num1, 1)
            self.assertEqual(abs_f2, f.name)
            self.assertEqual(num2, 2)

    def test_create_symlinks_to_input_files(self):
        with tempfile.TemporaryDirectory() as d:
            temp_path = os.path.join(d, "input_path")

            @path.create_symlinks_to_input_files(symlink_dir=temp_path)
            def test_function(arg1, arg2, arg3="", arg4=0):
                return arg1, arg2, arg3, arg4
            with tempfile.NamedTemporaryFile() as f1:
                with tempfile.NamedTemporaryFile() as f2:
                    f1_basename = os.path.basename(f1.name)
                    f2_basename = os.path.basename(f2.name)
                    abs_f1, num1, abs_f2, num2 = test_function(f1.name, 1, arg3=f2.name, arg4=2)
                    correct_outputs = (os.path.join(temp_path, f1_basename), os.path.join(temp_path, f2_basename))
                    self.assertTupleEqual((abs_f1, abs_f2), correct_outputs)
                    self.assertTupleEqual((num1, num2), (1, 2))
                    # test removal of original symlink
                    test_function(f1.name, 1, arg3=f2.name, arg4=2)

    def test_create_symlink_to_path(self):
        # Create temp dir to teset output path
        with tempfile.TemporaryDirectory() as d:
            with tempfile.NamedTemporaryFile() as f:
                symlink = path.create_symlink_to_path(f.name, d)
                self.assertTrue(os.path.islink(symlink))

    def test_working_directory(self):
        # get current directory
        cwd = os.getcwd()
        # Create temp dir to teset output path
        with tempfile.TemporaryDirectory() as d1:
            with tempfile.TemporaryDirectory() as d2:
                # in d2
                with path.working_directory(d2):
                    self.assertEqual(
                        os.path.abspath(os.getcwd()),
                        os.path.abspath(d2))
                # exit d2
            # in d1
            with path.working_directory(d1):
                self.assertEqual(
                    os.path.abspath(os.getcwd()),
                    os.path.abspath(d1))
            # exit d1
        # in orig
        self.assertEqual(
                os.path.abspath(os.getcwd()),
                os.path.abspath(cwd))

    def test_use_output_path_working_directory(self):
        # create temp dir
        with tempfile.TemporaryDirectory() as d:
            @path.use_output_path_working_directory
            def test_function(output_path):  # pylint: disable=unused-argument
                current_dir = os.getcwd()
                return current_dir

            def test_function2(output_path):  # pylint: disable=unused-argument
                current_dir = os.getcwd()
                return current_dir
            directory = test_function(d)
            directory2 = test_function2(d)
            self.assertEqual(directory, d)
            self.assertNotEqual(directory2, d)
