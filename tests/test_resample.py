import unittest
import os
import nibabel as nib
import numpy as np
from omni import resample
from . import TESTDATA


class TestResample(unittest.TestCase):
    def test_resample(self):
        # load test image
        img = nib.load(
            os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        )

        # test resample
        out_img = resample.resample(img, img, np.eye(4))

        # assert equality
        self.assertTrue(np.all(
            np.isclose(img.get_fdata(), out_img.get_fdata())))
