# pylint: disable=unused-import,redefined-outer-name
import os
import unittest
import numpy as np
import nibabel as nib
from omni import register
from omni.io import read_affine_file
from . import TESTDATA
from . import plumb_oblique_data


class TestRegister(unittest.TestCase):
    def test_grab_slice_encoding_dir(self):
        img_file = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        img = nib.load(img_file)
        assert register.grab_slice_encoding_dir(img) == 2

    def test_phase_corr_translate(self):
        ref = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        source = os.path.join(TESTDATA, "imgs", "test_plumb.nii.gz")
        ref_img = nib.load(ref)
        source_img = nib.load(source)
        affine = register.phase_corr_translate(ref_img, source_img)
        assert np.isclose(affine, np.eye(4)).all()


def test_register_main(tmp_path, plumb_oblique_data):
    test = plumb_oblique_data[0]
    test_params = tmp_path / "test_output"
    test_affine = tmp_path / "test_output.affine"
    test_r = tmp_path / "test_output_rimg.nii.gz"
    test_s = tmp_path / "test_output_simg.nii.gz"
    test_st = tmp_path / "test_output_stimg.nii.gz"
    register.register_main(
        target=test,
        interaction="none(0)",
        source=[test],
        output=str(test_params),
        regressed_output=str(test_r),
        blurred_regressed_output=str(test_s),
        aligned_output=str(test_st),
        tone_curve=True)

    # test affine
    affine_mat, atype = read_affine_file(str(test_affine))
    assert atype == "omni"
    assert np.all(np.isclose(affine_mat, np.eye(4)))

    # test images
    test_img = nib.load(test)
    test_rimg = nib.load(str(test_r))
    test_simg = nib.load(str(test_s))
    test_stimg = nib.load(str(test_st))
    assert test_rimg.shape == test_img.shape
    assert test_simg.shape == test_img.shape
    assert test_stimg.shape == test_img.shape
