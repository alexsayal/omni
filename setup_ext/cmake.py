import os
import sys
import re
import subprocess
import multiprocessing

from setuptools import Extension
from setuptools.command.build_ext import build_ext


class CMakeExtension(Extension):
    def __init__(self, name):
        Extension.__init__(self, name, sources=[])


class CMakeBuild(build_ext):
    def run(self):
        try:
            subprocess.run(['cmake', '--version'], check=True)
        except OSError:
            raise RuntimeError(
                "CMake must be installed to build the following extensions: " +
                ", ".join(e.name for e in self.extensions))

        # always use fresh build directory
        build_directory = os.path.abspath(self.build_temp)

        # define cmake args
        cmake_args = [
            '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + build_directory,
            '-DPYTHON_EXECUTABLE=' + sys.executable
        ]

        # get cpu count
        cpu_count = multiprocessing.cpu_count()

        # set number of jobs max(min(int(cpu_count/2), 8),1)
        jobs = max([min([int(cpu_count/2), 8]), 1])

        # Assuming Makefiles
        build_args = ["-j{}".format(jobs)]
        self.build_args = build_args

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(
            env.get('CXXFLAGS', ''),
            self.distribution.get_version())
        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)

        # CMakeLists.txt is in the same directory as this setup.py file
        cmake_list_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
        subprocess.run(['cmake', cmake_list_dir] + cmake_args,
                       cwd=self.build_temp, env=env, check=True)

        cmake_cmd = ['cmake', '--build', '.'] + self.build_args
        subprocess.run(cmake_cmd, cwd=self.build_temp, check=True)

        # Build omni cpp module
        ext = self.extensions[0]
        self.move_libs(ext)

    def move_libs(self, ext):
        # setup directory names
        build_temp = os.path.abspath(self.build_temp)
        dest_ext = self.get_ext_fullpath(ext.name)
        source_dir = os.path.join(build_temp, 'lib')
        dest_dir = os.path.dirname(dest_ext)
        os.makedirs(dest_dir, exist_ok=True)

        # make __init__.py
        with open(os.path.join(dest_dir, "__init__.py"), "w"):
            pass

        # move libs to destination path
        for f in os.listdir(source_dir):
            source_path = os.path.join(source_dir, f)
            dest_path = os.path.join(dest_dir, f)
            # skip armadillo noversion/maintenance versions
            if "libarmadillo.so" in f:
                # match major version lib
                match = re.search("libarmadillo.so.[0-9]{1,2}$", f)
                if not match:  # don't copy if not major version
                    continue
            self.copy_file(source_path, dest_path)
