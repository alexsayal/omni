# omni

omni is a Python/C++17 library for registering neuroimages and other related things. It provides useful and
convenient APIs for developing neuroimaging tools. It also contains implementations of software tools I am currently
developing.

Current software tools list:

- ### [Synth](#synth-1)

## Requirements

omni has only been tested on linux systems. You will need `python >=3.6`, `pip >= 19.1`, `cmake >=3.13`, `gcc >=8`,
`gfortran >=8`, `openblas`, `lapack`, `hdf5`, and `fftw3`. Example package manager commands are below:

### `Ubuntu` and `Debian`
``` shell
# this was tested on Ubuntu 20.04 and 18.04
apt-get install -y python3 python3-dev python3-distutils python3-pip \
                   cmake build-essential gfortran \
                   libopenblas-dev liblapack-dev libhdf5-dev libfftw3-dev
```
on `Ubuntu`, you may need to export the following to get `hdf5` to link properly:

``` shell
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH:+${CPLUS_INCLUDE_PATH}:}/usr/include/hdf5/serial
```

### `Centos`
``` shell
# this was tested on Centos 7

# add the epel repo
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# dependencies
yum -y install python3-devel git openblas-devel lapack-devel hdf5-devel fftw3-devel

# pip/cmake versions on Centos are old, so we use pip to update them to their latest versions
pip3 install pip cmake -U

# install devtoolset for gcc/gfortran 8
yum -y install centos-release-scl && yum -y install devtoolset-8

# this enables the gcc/gfortran toolset
source /opt/rh/devtoolset-8/enable
```

You will also need `nipy` for some scripts. There is a modified version of `nipy` meant for use with
omni [here](https://github.com/vanandrew/nipy/tree/0.4.2_dep_fix) (The official `nipy` package should also be ok,
but has not been thoroughly tested.):

``` shell
# install modified nipy dependency
pip install git+https://github.com/vanandrew/nipy.git@0.4.2_dep_fix
```

In addition to these libraries, omni currently relies on FSL, AFNI, and ANTs for some functionality.
The list below are the known versions for each package known to work with omni:
```
# Newer/Older versions may still work, but have not been tested.
FSL 6.0.4
AFNI 20.0.18
ANTs 15a6861
```
To install these packages, refer to each package's respective documenation.

## Installation

omni is under active development and does not have any prebuilt packages at the moment. A PyPI package
is planned once the library is more stable.

The recommended installation method is to clone this repo and use the provided `install` script to autobuild the
library. Note that you will still need to install system related dependencies as outlined in the
[Requirements](#requirements) section.
``` shell
# clone this repo
git clone https://gitlab.com/vanandrew/omni.git

# run the install script
# if you do not have root permissions, it is recommended
# to run ./install with the `--user` flag
./install # use -h for install options
```

## Installation (Developer)

If you are a developer, installing omni in editable mode may be convenient for testing. The `install` script has
a `--develop` option for this purpose:
``` shell
./install --develop # will install omni in editable mode
```

## Usage

There are various scripts in the `omni/scripts` folder. You can see each scripts `-h`
option for more details on usage. Scripts are prefixed by `omni_` and can be found on your path after
successful installation.

### Synth

`Synth` has two main scripts:

- `omni_synthtarget` - This script corresponds to algorithm 1 in the
[paper](https://www.biorxiv.org/content/10.1101/2021.03.13.435270v1). It generates a synthetic image using the
geometry of the source image(s) provided and the contrast properties of the target image given.

- `omni_synthunwarp` - This scripts corresponds to algorithm 2 in the
[paper](https://www.biorxiv.org/content/10.1101/2021.03.13.435270v1). It uses a synthetic image to correct EPI
distortion in an image while simultaneously aligning it to the source image(s) the synthetic image is based on.
