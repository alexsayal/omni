#!/usr/bin/env python3
import os
import sys
import setuptools
PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(PROJECT_DIR)
from setup_ext.cmake import CMakeExtension, CMakeBuild  # noqa, pylint: disable=wrong-import-position

SCRIPTSPATH = os.path.join(PROJECT_DIR, "omni", "scripts")

if __name__ == "__main__":
    setuptools.setup(
        ext_modules=[CMakeExtension("omni_cpp.omni")],
        cmdclass={"build_ext": CMakeBuild},
        entry_points={
            'console_scripts': [
                "omni_{0}=omni.scripts.{0}:main".format(
                    f.split(".")[0]) for f in os.listdir(SCRIPTSPATH)
                if "__pycache__" not in f and "__init__.py" not in f and "arguments.py" not in f]
        },
        package_data={
            "omni": ["pipelines/atlas/*"]
        }
    )
