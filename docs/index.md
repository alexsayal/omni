# omni Documentation

Documentation for the omni neuroimaging registration library.


```{toctree}
---
maxdepth: 2
---
installation
synth
api
```
