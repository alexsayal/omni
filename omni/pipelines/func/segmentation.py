import logging
from omni.interfaces.common import append_suffix, repath
from omni.interfaces.fsl import bet
from omni.path import create_output_path


@create_output_path
def brain_extraction(output_path: str,
                     ref_func_debias: str,
                     fractional_intensity_threshold: float = 0.5):
    """Brain extraction

        Parameters
        ----------
        output_path : str
            Output path to write out files to.
        ref_func_debias : str
            Reference functional.
        fractional_intensity_threshold : float
            Fractional intensity threshold for bet.

        Returns
        -------
        str
            Functional mask.
        str
            Brain extracted functional.
    """
    # run bet based on method selected
    logging.info("Running brain extraction...")
    ref_func_debias_bet = append_suffix(
        repath(output_path, ref_func_debias), "_bet")
    _, ref_func_debias_mask = bet(ref_func_debias_bet,  # pylint: disable=unbalanced-tuple-unpacking
                                  ref_func_debias,
                                  fractional_intensity_threshold,
                                  mask=True)

    # return files
    return (ref_func_debias_mask, ref_func_debias_bet)
