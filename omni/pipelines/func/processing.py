# pylint: disable=unused-wildcard-import
from typing import List, Dict
import nibabel as nib
from omni.path import *
from omni.register import grab_slice_encoding_dir
from ..common import Pipeline, redefine_result_key
from .stages import *


@create_output_path
@use_abspaths
@use_output_path_working_directory
@create_symlinks_to_input_files()
def func_proc(output_path: str,  # pylint: disable=dangerous-default-value
              func: str,
              TR: float,
              slice_times: List,
              fractional_intensity_threshold_func: float = 0.5,
              debias_params_func: str = "[200,3,1x1x1,3]",
              loops: List[int] = [1, 1, 1],
              subsample: List[int] = [5, 3, 1],
              **kwargs) -> Dict:
    """Functional processing pipeline

        Parameters
        ----------
        output_path : str
            Output path to write out files to.
        func : str
            Functional image.
        TR : float
            Repetition time of scan.
        slice_times : List
            List of slice times.
        fractional_intensity_threshold : float
            Fractional intensity threshold for bet.
        debias_params_func : str
            Custom spline fitting string.
        loops : List[int]
            Number of loops for SpaceTimeRealign.
        subsample : List[int]
            Subsampling for SpaceTimeRealign.

        Returns
        -------
        Dict
            Dictionary of results.
    """
    # grab the slice encoding direction (it's probably 2)
    sed = grab_slice_encoding_dir(nib.load(func))

    # create stages
    deoblique_func_stage = DeobliqueFuncStage()
    create_reference_and_moco_stage = CreateReferenceAndMocoStage(
        TR=TR, slice_times=slice_times, sed=sed,
        loops=loops, subsample=subsample)
    debias_stage = DebiasStage(spline_fit=debias_params_func)
    brain_extraction_stage = BrainExtractionStage(
        fractional_intensity_threshold=fractional_intensity_threshold_func)
    autobox_and_normalize_stage = AutoboxAndNormalizeStage()

    # create pipeline
    func_pipeline = Pipeline([
        ("start", deoblique_func_stage),
        (deoblique_func_stage, create_reference_and_moco_stage),
        (create_reference_and_moco_stage, debias_stage),
        (debias_stage, brain_extraction_stage),
        ((create_reference_and_moco_stage,
          debias_stage,
          brain_extraction_stage), autobox_and_normalize_stage),
        ])

    # run pipeline
    func_pipeline.run(func=func)

    # return results
    return func_pipeline.results


def func_to_epi_results(results: Dict) -> Dict:
    """Converts results of functional pipeline for input into EPI pipeline.

        Parameters
        ----------
        results : Dict
            Results dictionary from functional pipeline.

        Returns
        -------
        Dict
            Converted dictionary for EPI pipeline input.
    """
    # change keys
    results = redefine_result_key(
        results, "ref_func_debias_ab", "ref_epi")
    results = redefine_result_key(
        results, "ref_func_debias_bet_ab", "ref_epi_bet")
    results = redefine_result_key(
        results, "ref_func_debias_mask_ab", "ref_epi_bet_mask")
    results = redefine_result_key(
        results, "func_do_moco_ab", "epi")

    # return new results
    return results
