import os
import json
from typing import Dict, List
import nibabel as nib
from omni.path import *
from omni.interfaces.common import append_suffix, repath
from omni.pipelines.common import Pipeline, Stage
from omni.pipelines.anat.masks import weight_mask_and_autobox
from omni.pipelines.epi.stages import (AlignAffineEpiToAnatStage,
                                       DistortionCorrectionStage)
from .anat.processing import anat_proc, anat_to_epi_results
from .func.processing import func_proc, func_to_epi_results
from .epi.processing import epi_proc


@create_output_path
def pre_proc(output_path: str,
             anat_path: str = "anat",
             func_path: str = "func",
             epi_path: str = "epi",
             **kwargs) -> None:
    """Run Synth preprocessing pipeline.

        Parameters
        ----------
        output_path : str
            Output path to write out files to.
        anat_path : str
            Subpath for anatomical outputs.
        func_path : str
            Subpath for functional outputs.
        epi_path : str
            Subpath for EPI outputs.
        kwargs : dict
            Various keyword Arguments.
    """
    # run anatomical pipeline
    anat_results = anat_proc(os.path.join(output_path, anat_path), **kwargs)

    # open the metadata from the bids sidecar, if metadata not defined.
    if "metadata" not in kwargs:
        with open(kwargs["bids_sidecar"], "r") as metadata_file:
            metadata = json.load(metadata_file)
    else:
        metadata = kwargs["metadata"]

    # run functional pipeline
    func_results = func_proc(os.path.join(output_path, func_path),
                             TR=metadata["RepetitionTime"],
                             slice_times=metadata["SliceTiming"],
                             **kwargs)

    # delete old values
    ckwargs = kwargs.copy()
    del ckwargs["t1"]
    del ckwargs["t2"]

    # run EPI pipeline
    epi_proc(os.path.join(output_path, epi_path),
             **anat_to_epi_results(anat_results),
             **func_to_epi_results(func_results), **ckwargs)


@create_output_path
def make_ref_epi_bet(output_path: str,
                     ref_epi: str,
                     ref_epi_bet_mask: str,
                     **kwargs):  # pylint: disable=redefined-outer-name
    """Function for applying mask to ref epi

        Parameters
        ----------
        ref_epi : str
            Reference EPI.
        ref_epi_bet_mask : str
            Brain mask for EPI.

        Returns
        -------
        str
            Brain extracted EPI.
    """
    # create path
    ref_epi_bet = repath(output_path, append_suffix(ref_epi, "_bet"))

    # load data
    ref_epi_img = nib.load(ref_epi)
    ref_epi_bet_mask_img = nib.load(ref_epi_bet_mask)

    # write masked image to file
    nib.Nifti1Image(
        ref_epi_img.get_fdata()*ref_epi_bet_mask_img.get_fdata(),
        ref_epi_img.affine).to_filename(ref_epi_bet)

    # return
    return ref_epi_bet


@create_output_path
@use_abspaths
@use_output_path_working_directory
@create_symlinks_to_input_files()
def synthunwarp(output_path: str,  # pylint: disable=dangerous-default-value
                epi: str,
                ref_epi: str,
                ref_epi_bet_mask: str,
                anat_bet_mask: str,
                anat_eye_mask: str,
                t1_debias: str = None,
                t2_debias: str = None,
                initial_synth_model: str = "rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)",
                final_synth_model: str = "rbf(0;12)+rbf(1;12)+rbf(0;12)*rbf(1;12)",
                program: str = "fsl",
                bandwidth: int = 16,
                skip_affine: bool = False,
                resolution_pyramid: List[float] = [4, 2, 1],
                synthtarget_max_iterations: List[int] = [2000, 500, 100],
                synthtarget_err_tol: List[float] = [1e-4, 1e-4, 5e-4],
                synthtarget_step_size: List[float] = [1e-3, 1e-3, 1e-3],
                resample_resolution: float = 1,
                sigma_t2: float = 0.5,
                distortion_correction_step_size: List[float] = [3, 1, 0.1],
                noise_mask_dilation_size: int = 2,
                noise_mask_iterations: int = 20,
                noise_mask_sigma: float = 2,
                **kwargs) -> Dict:
    """SynthUnwarp Pipeline.

        Parameters
        ----------
        output_path : str
            Output path to write out files to.
        epi : str
            EPI image.
        ref_epi : str
            Reference EPI image.
        ref_epi_bet_mask : str
            Reference EPI brain mask.
        anat_bet_mask : str
            Anatomical brain mask.
        anat_eye_mask : str
            Anatomical eye mask.
        t1_debias : str
            Bias field corrected T1 image.
        t2_debias : str
            Bias field corrected T2 image.
        inital_synth_model : str
            Initial model used to generate synthetic image.
        final_synth_model : str
            Final model used to generate synthetic image.
        program : str
            Program to use for affine alignment.
        bandwidth : int
            Bandwidth for Epanechnikov kernel.
        skip_affine : bool
            Skip affine alignment step.
        resolution_pyramid : List[float]
            Resampling pyramid to use for affine alignment (mm).
        synthtarget_max_iterations : List[int]
            Max iterations for each SynthTarget call.
        synthtarget_err_tol: List[float]
            Error tolerance level for each SynthTarget call.
        synthtarget_step_size: List[float]
            Step size for gradient descent.
        resample_resolution : float
            Resample resolution space to do warps on (mm).
        sigma_t2 : float
            Parameter to smooth T2 for initial warp.
        distortion_correction_step_size : List[float]
            Set the gradient descent step size for each iteration of warp.
        noise_mask_dilation_size : int
            Dilation size for noise mask.
        noise_mask_iterations : int
            Number of iterations to run noise mask LDA.
        noise_mask_sigma : float
            Size of gaussian smoothing kernel for noise mask.

        Returns
        -------
        Dict
            Results of pipeline.
    """
    # make stages
    make_ref_epi_bet_stage = Stage(
        make_ref_epi_bet,
        stage_outputs=["ref_epi_bet"],
        hash_output="synthunwarp_0_make_ref_epi_bet",
        output_path="synthunwarp_0_make_ref_epi_bet",
        ref_epi=ref_epi,
        ref_epi_bet_mask=ref_epi_bet_mask)
    weight_mask_and_autobox_stage = Stage(
        weight_mask_and_autobox,
        stage_outputs=[
                "notused0",
                "anat_weight_mask",
                "anat_bet_mask",
                "notused1",
                "notused2",
                "t1",
                "t2",
                "t1_bet",
                "t2_bet"],
        hash_output="synthunwarp_1_weight_mask_and_autobox",
        output_path="synthunwarp_1_weight_mask_and_autobox",
        t1_debias=t1_debias,
        t2_debias=t2_debias,
        anat_bet_mask=anat_bet_mask,
        anat_eye_mask=anat_eye_mask)
    align_affine_epi_to_anat_stage = AlignAffineEpiToAnatStage(
        path="synthunwarp_2_align_affine_epi_to_anat",
        initial_synth_model=initial_synth_model,
        program=program,
        bandwidth=bandwidth,
        skip_affine=skip_affine,
        resolution_pyramid=resolution_pyramid,
        max_iterations=synthtarget_max_iterations,
        err_tol=synthtarget_err_tol,
        step_size=synthtarget_step_size)
    distortion_correction_stage = DistortionCorrectionStage(
        path="synthunwarp_3_distortion_correction",
        epi=epi, ref_epi=ref_epi,
        ref_epi_bet_mask=ref_epi_bet_mask,
        final_synth_model=final_synth_model,
        bandwidth=bandwidth,
        resample_resolution=resample_resolution,
        sigma_t2=sigma_t2,
        step_size=distortion_correction_step_size,
        noise_mask_dilation_size=noise_mask_dilation_size,
        noise_mask_iterations=noise_mask_iterations,
        noise_mask_sigma=noise_mask_sigma)

    # create pipeline
    pipeline = Pipeline([
        ("start", make_ref_epi_bet_stage),
        ("start", weight_mask_and_autobox_stage),
        ((make_ref_epi_bet_stage, weight_mask_and_autobox_stage), align_affine_epi_to_anat_stage),
        ((weight_mask_and_autobox_stage, align_affine_epi_to_anat_stage), distortion_correction_stage)])

    # run pipeline
    pipeline.run()

    # return results
    return pipeline.results
