from typing import Dict
from omni.path import *
from ..common import Pipeline, redefine_result_key
from .stages import *


@create_output_path
@use_abspaths
@use_output_path_working_directory
@create_symlinks_to_input_files()
def anat_proc(
        output_path: str,
        t1: str,
        t2: str,
        ref: str = "T1",
        program: str = "fsl",
        atlas: str = "mni",
        use_eye_mask: bool = True,
        debias_params_anat: str = "[100,3,1x1x1,3]",
        fractional_intensity_threshold_anat: float = 0.5,
        bet_method: str = "Norm",
        **kwargs) -> Dict:
    """Anatomical processing pipeline.

        Parameters
        ----------
        output_path : str
            Output path to write out files to.
        t1 : str
            T1 image.
        t2 : str
            T2 image.
        ref : str
            Set the image to use as a reference [T1/T2].
        program : str
            Program to use for alignment
            ('afni': AFNI 3dAllineate or 'fsl': fsl flirt).
        atlas : str
            Atlas to align to.
        use_eye_mask : str
            Enhance anatomical weight mask with eye mask.
        debias_params_anat : str
            Custom spline fitting string.
        fractional_intensity_threshold_anat : float
            Set fractional intensity threshold for bet.
        bet_method : str
            Method of brain extraction.

        Returns
        -------
        Dict
            Dictionary of results.
    """
    # create stages
    deoblique_anat_stage = DeobliqueAnatStage()
    debias_stage = DebiasStage(spline_fit=debias_params_anat)
    align_anat_stage = AlignAnatStage(ref=ref, program=program)
    brain_extraction_stage = BrainExtractionStage(
        use_eye_mask=use_eye_mask,
        fractional_intensity_threshold=fractional_intensity_threshold_anat,
        method=bet_method)
    align_atlas_stage = AlignAtlasStage(atlas=atlas, ref=ref, program=program)
    weight_mask_and_autobox_stage = WeightMaskAndAutoboxStage()

    # create pipeline
    anat_pipeline = Pipeline([
        ("start", deoblique_anat_stage),
        (deoblique_anat_stage, debias_stage),
        (debias_stage, align_anat_stage),
        (align_anat_stage, brain_extraction_stage),
        ((brain_extraction_stage,
          align_anat_stage,
          debias_stage), align_atlas_stage),
        ((brain_extraction_stage,
          align_anat_stage), weight_mask_and_autobox_stage)
        ])

    # run pipeline
    anat_pipeline.run(t1=t1, t2=t2)

    # results
    return anat_pipeline.results


def anat_to_epi_results(results: Dict) -> Dict:
    """Converts results of anatomical pipeline for input into EPI pipeline.

        Parameters
        ----------
        results : Dict
            Results dictionary from anatomical pipeline.

        Returns
        -------
        Dict
            Converted dictionary for EPI pipeline input.
    """
    # change keys
    results = redefine_result_key(
        results, "weight_mask_ab", "anat_weight_mask")
    results = redefine_result_key(
        results, "anat_bet_mask_ab", "anat_bet_mask")
    results = redefine_result_key(
        results, "t1_debias_ab_sat_lce", "t1")
    results = redefine_result_key(
        results, "t2_debias_ab_sat_lce", "t2")
    results = redefine_result_key(
        results, "t1_debias_ab_sat_lce_bet", "t1_bet")
    results = redefine_result_key(
        results, "t2_debias_ab_sat_lce_bet", "t2_bet")

    # return new results
    return results
