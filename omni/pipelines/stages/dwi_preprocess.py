import os
import logging
import numpy as np
import nibabel as nib
from omni import affine, io
# from .common import hash_checker, run_process

# define internal filenames for stage
dwi_preprocess_pd = {
    "dwi_do": "dwi_deobliqued.nii.gz",
    "ap_do": "ap_deobliqued.nii.gz",
    "pa_do": "pa_deobliqued.nii.gz",
    "merged": "merged.nii.gz",
    "topup_output": "topup_output",
    "topup_ioutput": "topup_ioutput.nii.gz",
    "datain": "datain.txt",
    "index": "index.txt",
    "unwarped_avg_dwi": "unwarped_avg_dwi.nii.gz",
    "unwarped_avg_dwi_bet": "unwarped_avg_dwi_bet.nii.gz",
    "unwarped_avg_dwi_mask": "unwarped_avg_dwi_bet_mask.nii.gz",
    "dwi_ec": "dwi_eddycorrected.nii.gz",
    "ref_dwi": "ref_dwi.nii.gz",
    "ref_dwi_debias": "ref_dwi_debias.nii.gz",
    "ref_dwi_debias_nm": "ref_dwi_debias_nm.nii.gz",
    "ref_dwi_debias_nm_bet": "ref_dwi_debias_nm_bet.nii.gz",
    "dwi_b0_set": "dwi_b0_set.nii.gz",
    # create keys to pass into align_anat_func
    "func_framewise_aligned": "dwi_b0_set.nii.gz",
    "ref_func_debias_nm": "ref_dwi_debias_nm.nii.gz",
    "ref_func_debias_nm_bet": "ref_dwi_debias_nm_bet.nii.gz",
    # create key for compose register
    "func_do": "dwi_eddycorrected.nii.gz",
    "func_xfm": "func_xfm.aff12.1D"
}

# setup hash check decorator
# @hash_checker({'dwi_preprocess': os.path.abspath(__file__)})
def dwi_preprocess(pd, metadataAP, metadataPA, dwi_debias, fractional_intensity_threshold):
    """
        Preprocess the dwi image
    """
    logging.info('Deobliquing diffusion image...')

    # deoblique dwi
    dwi_img = nib.load(pd['dwi'])
    dwi_deobliqued = affine.deoblique(dwi_img)

    # write out the deobliqued image
    dwi_deobliqued.to_filename(pd['dwi_do'])

    # 3d autobox the dwi image
    run_process(
        "3dAutobox -input {} -prefix {} -npad 5 -overwrite".format(
            pd['dwi_do'],
            pd['dwi_do']
        )
    )

    # make sure dims are even
    dwi_do = nib.load(pd['dwi_do'])
    d0 = dwi_do.shape[0] if dwi_do.shape[0] % 2 == 0 else (dwi_do.shape[0] - 1)
    d1 = dwi_do.shape[1] if dwi_do.shape[1] % 2 == 0 else (dwi_do.shape[1] - 1)
    d2 = dwi_do.shape[2] if dwi_do.shape[2] % 2 == 0 else (dwi_do.shape[2] - 1)
    nib.Nifti1Image(dwi_do.get_fdata()[:d0,:d1,:d2], dwi_do.affine).to_filename(pd['dwi_do'])

    # now grab metadata from each fmap file
    pedAP = metadataAP['PhaseEncodingDirection']
    pedPA = metadataPA['PhaseEncodingDirection']
    try:
        readouttimeAP = metadataAP['TotalReadoutTime']
        readouttimePA = metadataPA['TotalReadoutTime']
    except KeyError:
        readouttimeAP = (metadataAP['ReconMatrixPE']-1) * metadataAP['EffectiveEchoSpacing']
        readouttimePA = (metadataPA['ReconMatrixPE']-1) * metadataPA['EffectiveEchoSpacing']
    
    # setup ped info
    ped_dict = {
        'i': '1 0 0',
        'j': '0 1 0',
        'k': '0 0 1',
        'i-': '-1 0 0',
        'j-': '0 -1 0',
        'k-': '0 0 -1'
    }

    # write datain file
    with open(pd['datain'], 'w') as f:
        f.write('{} {}\n'.format(ped_dict[pedAP], readouttimeAP))
        f.write('{} {}\n'.format(ped_dict[pedPA], readouttimePA))

    # deoblique the ap/pa fmap files
    ap_fmap = nib.load(pd['ap_fmap'])
    ap_do = affine.deoblique(ap_fmap)
    ap_do.to_filename(pd['ap_do'])
    pa_fmap = nib.load(pd['pa_fmap'])
    pa_do = affine.deoblique(pa_fmap)
    pa_do.to_filename(pd['pa_do'])

    # use same grid as dwi_do
    run_process(
        "3dresample -master {} -input {} -prefix {} -rmode Cu -overwrite".format(
            pd['dwi_do'],
            pd['ap_do'],
            pd['ap_do']
        )
    )
    run_process(
        "3dresample -master {} -input {} -prefix {} -rmode Cu -overwrite".format(
            pd['dwi_do'],
            pd['pa_do'],
            pd['pa_do']
        )
    )

    # merge ap/pa fmap files
    fslmerge = lambda ap,pa: run_process('fslmerge -t {} {} {}'.format(pd['merged'], ap, pa))
    fslmerge(pd['ap_do'], pd['pa_do'])

    # run top up
    topup = lambda m,d,out,iout: run_process('topup --imain={} --datain={} --config=b02b0.cnf --out={} --iout={} -v'.format(m, d, out, iout))
    topup(pd['merged'], pd['datain'], pd['topup_output'], pd['topup_ioutput'])

    # make average unwarped dwi image
    unwarped_dwi = nib.load(pd['topup_ioutput'])
    unwarped_avg_dwi = nib.Nifti1Image(np.mean(unwarped_dwi.get_fdata(), axis=3), unwarped_dwi.affine)
    unwarped_avg_dwi.to_filename(pd['unwarped_avg_dwi'])

    # run bet
    run_process(
        "bet {} {} -m -f {} -v".format(
            pd['unwarped_avg_dwi'],
            pd['unwarped_avg_dwi_bet'],
            fractional_intensity_threshold
        )
    )

    # write out index file for direction of dwi file
    # TODO: Assume AP for now, in the future extract from data
    num_vols = dwi_deobliqued.shape[3]
    with open(pd['index'], 'w') as f:
        for _ in range(num_vols):
            f.write('2 ')

    # get basename of dwi
    get_basename = lambda filename: os.path.splitext(filename)[0] if os.path.splitext(filename)[1] == '.nii' else get_basename(os.path.splitext(filename)[0])
    bvec = get_basename(pd['dwi']) + '.bvec'
    bval = get_basename(pd['dwi']) + '.bval'

    # run eddy
    run_process(
        "eddy_openmp --imain={} --mask={} --acqp={} --index={} --bvecs={} --bvals={} --topup={} --out={} --data_is_shelled -v".format(
            pd['dwi_do'],
            pd['unwarped_avg_dwi_mask'],
            pd['datain'],
            pd['index'],
            bvec, bval,
            pd['topup_output'],
            pd['dwi_ec']
        )
    )

    # create reference volume from first volume of e-corrected dwi
    dwi_ec = nib.load(pd['dwi_ec'])
    nib.Nifti1Image(dwi_ec.get_fdata()[:,:,:,0], dwi_ec.affine).to_filename(pd['ref_dwi'])

    # debias the ref
    run_process(
        "N4BiasFieldCorrection 3 -b '{}' -i {} -o {} -v".format(
            dwi_debias,
            pd['ref_dwi'],
            pd['ref_dwi_debias']
        )
    )

    # normalize the ref_dwi_debias
    normalize = lambda data: (data - data.ravel().min())/(data.ravel().max() - data.ravel().min())
    ref = nib.load(pd['ref_dwi_debias'])
    nib.Nifti1Image(normalize(ref.get_fdata()), ref.affine).to_filename(pd['ref_dwi_debias_nm'])

    # skullstrip the ref dwi
    run_process(
        'bet {} {} -m -f {} -v'.format(
            pd['ref_dwi_debias_nm'],
            pd['ref_dwi_debias_nm_bet'],
            fractional_intensity_threshold
        )
    )

    # grab only the B0 frames for noise mask creation later on
    with open(bval,'r') as f:
        bvals = [int(i) for i in f.read().rstrip().split(" ")]
    selected_volumes = dwi_ec.get_fdata()[:,:,:,np.array(bvals) == 0]
    nib.Nifti1Image(selected_volumes, dwi_ec.affine).to_filename(pd['dwi_b0_set'])

    # create a fake affine by outputting an identity matrix for framewise alignment
    io.write_afni_affine(pd['func_xfm'], np.eye(4))
