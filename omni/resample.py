import numpy as np
import nibabel as nib
from .interface import OmniInterface


def resample(target: nib.Nifti1Image,
             source: nib.Nifti1Image,
             affine: np.ndarray) -> nib.Nifti1Image:
    # setup interface
    oi = OmniInterface()
    oi.set_img("target", target)
    oi.set_img("source", source)

    # resample the image
    oi.link.resample("target", "source", "output", affine)

    # return output array (using header information of target)
    output_img = oi.get_img("output", ref=target)

    # return output image
    return output_img
