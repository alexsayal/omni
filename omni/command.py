#!/usr/bin/env python3
"""
    Adds common command line arguments for omni scripts
"""
import os
import logging
import sys
import argparse

# set version
__version__ = 'development'


def set_common(parser: argparse.ArgumentParser, threads: bool = True) -> None:
    """Adds common arguments for all scripts to parser."""
    # EASTER EGG
    if '--easter' in sys.argv[1:]:
        print("\nThis blue eye perceives all things conjoined. \n"
              "The past, the future, and the present. \n"
              "Everything flows and all is connected. \n"
              "This eye is not merely seen reality. \n"
              "It is touching the truth. \n")
        sys.exit(0)

    # set thread use
    if threads:
        parser.add_argument('-n', '--number_of_threads',
                            default=8,
                            type=int,
                            help='Sets number of openmp threads (default: 8)')

    # add version to command line
    parser.add_argument(
        '-v', '--version',
        action='version',
        help="Show program's version number and exit.",
        version='%s' % __version__)


def set_env(args: argparse.Namespace) -> None:
    """
        Sets Environment Variables
    """
    # set number of threads to use
    if args.number_of_threads:
        os.environ['OMP_NUM_THREADS'] = str(args.number_of_threads)
        os.environ['OPENBLAS_NUM_THREADS'] = str(args.number_of_threads)
        os.environ['ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS'] = str(args.number_of_threads)
