"""
    Module containing functions for adding arguments to scripts.

    Some scripts will have similar arguments. By combining all
    available arguments in one file, we can create a common interface
    for all scripts.
"""
from argparse import ArgumentParser
from typing import Callable


class Argument:  # pylint: disable=too-few-public-methods
    """Argument for ArgumentParser.
    
        Methods
        -------
        create:
    """
    @classmethod
    def create(cls, flag_name: str, default_help: str = "", **kwargs) -> Callable:
        """Create an argument for ArgumentParser.

            Parameters
            ----------
            flag_name : str
                Name of argument flag to add.
            default_help : str
                Default help text to use.

            Returns
            -------
            Callable
                A function to call with parser to add the argument.
        """
        # rename class kwargs to not conflict with function kwargs
        class_kwargs = kwargs

        # construct add_argument function
        def add_argument(parser: ArgumentParser, *args, positional_arg: bool = False, **kwargs):
            # remove help from kwargs if it exists
            func_kwargs = kwargs.copy()
            if "help" in func_kwargs:
                del func_kwargs["help"]

            # add argument to parser
            parser.add_argument(
                *args, "--%s" % flag_name if not positional_arg else flag_name,
                help=default_help if "help" not in kwargs else kwargs["help"],
                **class_kwargs,
                **func_kwargs)

        # return the argument function
        return add_argument


arg_affine = Argument.create("affine", "Affine tranform (omni format).")
arg_afnify = Argument.create("afnify", "Convert input to afni affine format.", action="store_true")
arg_anat_bet_mask = Argument.create("anat_bet_mask", "Brain mask in anatomical space.")
arg_anat_eye_mask = Argument.create("anat_eye_mask", "Eye mask in anatomical space.")
arg_anat_weight_mask = Argument.create(
    "anat_weight_mask", "An anatomical brain mask that is weighted around the skull.")
arg_atlas = Argument.create(
    "atlas", "Atlas to align to (Default: mni).",
    default="mni", choices=["mni"])
arg_bandwidth = Argument.create(
    "bandwidth", "Bandwidth for Epanechnikov kernel (Default: 16).", default=16, type=float)
arg_bet_method = Argument.create(
    "bet_method", "Brain extraction method (Default: Norm).", default="Norm", choices=["Norm", "T1"])
arg_bids_path = Argument.create(
    "bids_path",
    "The directory with the input dataset formatted according to the BIDS standard.")
arg_bids_sidecar = Argument.create(
    "bids_sidecar", "A BIDS sidecar .json file containing metadata related to the EPI image.")
arg_combine_sessions = Argument.create(
    "combine_sessions", "Combine sessions if a session is missing an anatomical image.", action="store_true")
arg_database = Argument.create("database", "Set database path for BIDS input (Default is to use BIDS input path).")
arg_data_resolution = Argument.create(
    "data_resolution", "Resolution to resample the data for output (mm).")
arg_debias_params_anat = Argument.create(
    "debias_params_anat",
    "BSpline fit option for N4BiasFieldCorrection (Anat) (Default: [100,3,1x1x1,3]).",
    default="[100,3,1x1x1,3]")
arg_debias_params_dwi = Argument.create(
    "debias_params_dwi",
    "BSpline fit option for N4BiasFieldCorrection (Dwi) (Default: [200,3,3x3x3,3]).",
    default="[200,3,3x3x3,3]")
arg_debias_params_func = Argument.create(
    "debias_params_func",
    "BSpline fit option for N4BiasFieldCorrection (Func) (Default: [200,3,3x3x3,3]).",
    default="[200,3,3x3x3,3]")
arg_dims = Argument.create(
    "dims", "Dims to reshape columns into.",
    nargs=3, default=[256, 256, 224], type=int)
arg_distortion_correction_step_size = Argument.create(
    "distortion_correction_step_size",
    "Gradient descent step sizes for distortion correction (Default: [3, 1, 0.1]).",
    default=[3, 1, 0.1], nargs="+", type=float)
arg_dryrun = Argument.create("dryrun", "Run pipeline without any outputs.", action="store_true")
arg_epi = Argument.create("epi", "An EPI image that is aligned across all frames.")
arg_err_tol = Argument.create(
    "err_tol", "Error tolerance for optimization solution (Default: 1e-4).",
    default=1e-4, type=float)
arg_final_dwi_resolution = Argument.create(
    "final_dwi_resolution",
    "Specify final resample resolution for dwi images (Default: 0 (Native Resolution)).",
    default=0, type=float)
arg_final_func_res = Argument.create(
    "final_func_resolution",
    "Specify final resample resolution for functional images (Default: 0 (Native Resolution)).",
    default=0, type=float)
arg_final_synth_model = Argument.create(
    "final_synth_model",
    "Model used in distorion correction stage (Default: 'rbf(0;12)+rbf(1;12)+rbf(0;12)*rbf(1;12)' ).",
    default='rbf(0;12)+rbf(1;12)+rbf(0;12)*rbf(1;12)')
arg_fixed_regress = Argument.create(
    "fixed_regress", "Fix the regression parameters with given regress file "
    "(must be dimensionally consistent with the model).")
arg_fourd_img = Argument.create("fourd_img", "A 4D image for joint slice time/motion correction.")
arg_fractional_intensity_threshold_anat = Argument.create(
    "fractional_intensity_threshold_anat",
    "Fractional intensity threshold for brain extraction tool (Anat) (Default: 0.5).",
    default=0.5, type=float)
arg_fractional_intensity_threshold_dwi = Argument.create(
    "fractional_intensity_threshold_dwi",
    "Fractional intensity threshold for brain extraction tool (Dwi) (Default: 0.5).",
    default=0.5, type=float)
arg_fractional_intensity_threshold_func = Argument.create(
    "fractional_intensity_threshold_func",
    "Fractional intensity threshold for brain extraction tool (Func) (Default: 0.5).",
    default=0.5, type=float)
arg_fslify = Argument.create(
    "fslify", "Convert input to fsl affine format (must provide target/source arguments).",
    action="store_true")
arg_func = Argument.create("func", "Functional Image.")
arg_initial_synth_model = Argument.create(
    "initial_synth_model",
    "Model used in SynthTarget stage (Default: 'rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)' ).",
    default='rbf(0;4)+rbf(1;4)+rbf(0;4)*rbf(1;4)')
arg_input = Argument.create("input", "Path to input file.")
arg_input_affine = Argument.create("input_affine", "Input affine file to convert.")
arg_input_image = Argument.create("input_image", "Input image.")
arg_input_images = Argument.create("input_images", "Input images to construct interaction matrix on.", nargs="+")
arg_interaction_model = Argument.create("interaction_model", "A string or .model file specifying how "
                                        "the interation operator should be constructed (F).")
arg_invert = Argument.create("invert", "Invert the provided affine file.", action="store_true")
arg_limits = Argument.create("limits",
                             "extraction limits (should be specified as '[x1:x2,y1:y2,z1:y2,...]'"
                             "Must match the dimensions of the source image, this basically just uses numpy indexing.")
arg_log_file = Argument.create("log_file", "Write log file to path.")
arg_loops = Argument.create(
    "loops",
    "Number of loops for SpaceTimeRealign (Default: [1, 1, 1]).",
    default=[1, 1, 1], nargs="+", type=int)
arg_max_iterations = Argument.create(
    "max_iterations", "Max iterations for optimization (Default: 200).",
    default=200, type=int)
arg_noise_mask_dilation_size = Argument.create(
    "noise_mask_dilation_size", "Dilation size for noise mask (Default: 2).", default=2, type=int)
arg_noise_mask_iterations = Argument.create(
    "noise_mask_iterations",
    "Number of iterations to run noise mask LDA (Default: 12).",
    default=12, type=int)
arg_noise_mask_sigma = Argument.create(
    "noise_mask_sigma",
    "Size of gaussian smoothing kernel for noise mask (Default: 2).", default=2, type=float)
arg_no_register = Argument.create("no_register", "Skips registration procedure.", action="store_true")
arg_omnify = Argument.create("omnify", "Convert input to omni affine format.", action="store_true")
arg_output = Argument.create("output", "Path to output file.")
arg_output_image = Argument.create("output_image", "Output image.")
arg_output_intmat = Argument.create("output_intmat", "Output interaction matrices.", action="store_true")
arg_output_path = Argument.create("output_path", "The directory where output files should be stored.")
arg_output_prefix = Argument.create("output_prefix", "A prefix for all the output files.")
arg_participant_label = Argument.create(
    "participant_label",
    'The label(s) of the participant(s) that should be analyzed. The label '
    'corresponds to sub-<participant_label> from the BIDS spec '
    '(so it does not include "sub-"). If this parameter is not '
    'provided all subjects should be analyzed. Multiple '
    'participants can be specified with a space separated list.', nargs="+")
arg_phase_encoding_direction = Argument.create(
    "phase_encoding_direction",
    "Sets the phase encoding direction (Default: none).",
    choices=["x", "y", "z", "none"], default="none")
arg_program = Argument.create(
    "program",
    "Alignment program to use (Default: fsl).",
    choices=["fsl", "afni"], default="fsl")
arg_ref = Argument.create("ref", "Anatomical Reference to use for T1/T2 alignment (Default: T1).", default="T1")
arg_ref_epi = Argument.create("ref_epi", "Reference EPI image (Single frame and [0,1] normalized).")
arg_ref_epi_bet_mask = Argument.create("ref_epi_bet_mask", "Brain mask of reference EPI image.")
arg_ref_idx = Argument.create(
    "ref_idx",
    "Index of reference to use for SpaceTimeRealign (Default: 0).",
    default=0, type=int)
arg_regression_weight_mask = Argument.create(
    "regression_weight_mask", "Regression weight mask for regression model.")
arg_regression_output = Argument.create(
    "regression_output", "SynthTarget regression output (Before blurring operator).")
arg_reset_database = Argument.create(
    "reset_database",
    "Delete BIDS database files if they exist.",
    action="store_true")
arg_resample_resolution = Argument.create(
    "resample_resolution", "Resample resolution space to do warps on (mm) (Default: 1).",
    default=1, type=float)
arg_resolution_pyramid = Argument.create(
    "resolution_pyramid", "Resampling pyramid to use for affine alignment (mm) (Default: [4, 2, 1]).",
    default=[4, 2, 1], nargs="+", type=float)
arg_rigid_transform = Argument.create(
    "rigid_transform",
    "Generates a omni affine that does the specified transform (angles are in degrees, "
    "translations in physical coordinates (usually mm).", metavar=('ANGLEX', 'ANGLEY', 'ANGLEZ', 'TX', 'TY', 'TZ'),
    nargs=6, type=float)
arg_sigma_t2 = Argument.create(
    "sigma_t2",
    "Size of gaussian smoothing kernel for T2 image (Default: 0.5).", default=0.5, type=float)
arg_skip_affine = Argument.create(
    "skip_affine", "Skip affine registration (Uses identity matrix).", action="store_true")
arg_skip_database = Argument.create("skip_database", "Skip database construction.", action="store_true")
arg_skip_validation = Argument.create("skip_validation", "Skip validation of BIDS input.", action="store_true")
arg_source = Argument.create("source", "Specifies source image to calculate fsl affine over.")
arg_step_size = Argument.create(
    "step_size", "Step size for optimization (Default: 1e-3).", default=1e-3, type=float)
arg_subsample = Argument.create(
    "subsample",
    "Subsampling for SpaceTimeRealign (Default: [5, 3, 1]).",
    default=[5, 3, 1], nargs="+", type=int)
arg_synthtarget_err_tol = Argument.create(
    "synthtarget_err_tol", "Error tolerance for each SynthTarget call (Default: [1e-4, 1e-4, 5e-4]).",
    default=[1e-4, 1e-4, 5e-4], nargs="+", type=float)
arg_synthtarget_max_iterations = Argument.create(
    "synthtarget_max_iterations", "Max iterations for each SynthTarget call (Default: [2000, 500, 100]).",
    default=[2000, 500, 100], nargs="+", type=int)
arg_synthtarget_step_size = Argument.create(
    "synthtarget_step_size", "Step size for SynthTarget calls (Default: [1e-3, 1e-3, 1e-3]).",
    default=[1e-3, 1e-3, 1e-3], nargs="+", type=float)
arg_synth_image = Argument.create("synth_image", "Synthetic image.")
arg_synth_image_targetspace = Argument.create("synth_image_targetspace", "Synthetic image aligned to target space.")
arg_t1 = Argument.create("t1", "A T1 image.")
arg_t2 = Argument.create("t2", "A T2 image.")
arg_t1_debias = Argument.create("t1_debias", "A T1 image that is bias field corrected.")
arg_t2_debias = Argument.create("t2_debias", "A T2 image that is bias field corrected.")
arg_target = Argument.create("target", "Specifies target image to calculate fsl affine over.")
arg_target_image = Argument.create("target_image", "Target image.")
arg_test_mode = Argument.create("test_mode", "Runs a single session, then quits.", action="store_true")
arg_tone_curve = Argument.create(
    "tone_curve", "Apply tone curve adjustment.", action="store_true")
arg_tone_curve_mask = Argument.create(
    "tone_curve_mask", "Mask for tone curve mapping (in input image(s) space).")
arg_use_default_anatomical = Argument.create(
    "use_default_anatomical",
    "If an anatomical image is missing in a session, enabling this flag will assign the "
    "first availiable anatomical to the session.", action="store_true")
arg_warp = Argument.create("warp", "Forward warp (anat to epi; from the space where anat is affine aligned "
                           "to the epi).")
