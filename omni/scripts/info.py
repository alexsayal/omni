#!/usr/bin/env python3
import argparse
import nibabel as nib
from omni import command
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description='Get image info',
    epilog='Author: Andrew Van, vanandrew@wustl.edu, 10/20/2020')
arg_input_image(parser, positional_arg=True)
# set common arguments
command.set_common(parser, False)


def main():
    # call parser
    args = parser.parse_args()

    # read image
    img = nib.load(args.input_image)

    # print header
    print(img.header)
