from pathlib import Path
import argparse
import tempfile
import numpy as np
import nibabel as nib
from omni.command import set_common
from omni.io import convert_affine_file
from omni.interfaces.afni import NwarpCat
from omni.masks import make_regression_mask
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description="Generates a weighted regression mask for use in Synth.",
    epilog="Author: Andrew Van, vanandrew@wustl.edu, 03/29/2021")
arg_output_path(parser, "-o", required=True)
arg_epi(parser, "-e", required=True)
arg_anat_bet_mask(parser, "-m", required=True)
arg_anat_weight_mask(parser, "-w", required=True)
arg_affine(parser, "-a", help="Affine transform (anat to epi) (afni format).", required=True)
arg_warp(parser, "-f")
arg_noise_mask_dilation_size(parser, "-d")
arg_noise_mask_iterations(parser, "-i")
arg_noise_mask_sigma(parser, "-s")
# set common arguments
set_common(parser, False)


def main():
    # call parser
    args = parser.parse_args()

    # create a temp directory context
    with tempfile.TemporaryDirectory() as d:
        # create an inverse affine filename
        args.iaffine = str(Path(d) / "inverse_affine.aff12.1D")

        # get the inverse of the affine file
        convert_affine_file(args.iaffine, args.affine, "afni", invert=True)

        # if warp not specfied, create an empty warp field
        if not args.warp:
            # get epi image 3D shape
            epi_img = nib.load(args.epi)
            epi_img_shape = epi_img.shape[:3] + (3,)

            # make empty numpy array with shape
            warp_data = np.zeros(epi_img_shape)
            warp_img = nib.Nifti1Image(warp_data, epi_img.affine)
            args.warp = str(Path(d) / "warp.nii.gz")
            warp_img.to_filename(args.warp)

        # make inverse warp
        args.iwarp = str(Path(d) / "iwarp.nii.gz")
        NwarpCat(args.iwarp, args.warp, True)

        # make output path folders
        Path(args.output_path).mkdir(parents=True, exist_ok=True)

        # call make regression mask
        arguments = vars(args)
        arguments["output_prefix"] = str(Path(args.output_path) / "synth_")
        del arguments["output_path"]
        regression_mask = make_regression_mask(**arguments)
        print("Regression mask at: %s" % regression_mask)
