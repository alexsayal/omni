import argparse
import nibabel as nib
from omni.command import set_common
from omni.masks import generate_noise_mask
from omni.interfaces.common import append_suffix
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description='Generates a noise mask',
    epilog='Author: Andrew Van, vanandrew@wustl.edu, 01/26/2021')
arg_epi(parser, positional_arg=True)
arg_ref_epi_bet_mask(parser, positional_arg=True)
arg_output(parser, positional_arg=True)
arg_noise_mask_dilation_size(parser, "-d")
arg_noise_mask_iterations(parser, "-i")
arg_noise_mask_sigma(parser, "-k")
# set common arguments
set_common(parser, False)


def main():
    # call parser
    args = parser.parse_args()

    # load up image files
    epi = nib.load(args.epi)
    mask = nib.load(args.ref_epi_bet_mask)

    # generate noise mask
    noise_mask, noise_mask_smooth = generate_noise_mask(
        epi, mask, args.noise_mask_dilation_size,
        args.noise_mask_iterations, args.noise_mask_sigma)

    # save out files
    noise_mask.to_filename(args.output)
    noise_mask_smooth.to_filename(append_suffix(args.output, "_smooth"))
