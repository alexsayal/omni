#!/usr/bin/env python3
import argparse
import logging
from omni import command
from omni.pipelines.bids import bids_proc
from omni.pipelines.logging import setup_logging
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description='Runs omni alignment pipeline.',
    epilog='Author: Andrew Van, vanandrew@wustl.edu, 03/27/2021')
arg_bids_path(parser, positional_arg=True)
arg_output_path(parser, positional_arg=True)
arg_participant_label(parser)
arg_dryrun(parser)
arg_combine_sessions(parser)
arg_skip_validation(parser)
arg_database(parser)
arg_reset_database(parser)
arg_skip_database(parser)
arg_log_file(parser)
arg_program(parser)
arg_bet_method(parser)
arg_debias_params_anat(parser)
arg_debias_params_func(parser)
arg_fractional_intensity_threshold_anat(parser)
arg_fractional_intensity_threshold_func(parser)
arg_loops(parser, "-l")
arg_subsample(parser)
arg_initial_synth_model(parser)
arg_final_synth_model(parser)
arg_bandwidth(parser, "-p")
arg_skip_affine(parser)
arg_resolution_pyramid(parser)
arg_synthtarget_max_iterations(parser, "-d")
arg_synthtarget_err_tol(parser)
arg_synthtarget_step_size(parser)
arg_resample_resolution(parser)
arg_sigma_t2(parser)
arg_distortion_correction_step_size(parser)
arg_noise_mask_dilation_size(parser)
arg_noise_mask_iterations(parser)
arg_noise_mask_sigma(parser, "-s")
arg_data_resolution(parser)
arg_atlas(parser)
# set common arguments
command.set_common(parser)


def main():
    # call parser
    args = parser.parse_args()

    # setup env
    command.set_env(args)

    # setup logging
    setup_logging(args.log_file)

    # log arguments
    logging.info(args)

    # argument checks
    assert len(args.loops) == len(args.subsample)
    assert len(args.resolution_pyramid) == len(args.synthtarget_max_iterations)
    assert len(args.synthtarget_max_iterations) == len(args.synthtarget_err_tol)
    assert len(args.synthtarget_max_iterations) == len(args.synthtarget_step_size)

    # pass to bids processor
    bids_proc(**vars(args))
