#!/usr/bin/env python3
"""
    Script to deoblique images
"""
import argparse
import nibabel as nib
from omni import command, affine
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description='Deobliques an image',
    epilog='Author: Andrew Van, vanandrew@wustl.edu, 02/25/2020')
arg_input_image(parser, positional_arg=True, help="Image to deoblique.")
arg_output_image(parser, positional_arg=True, help="Deobliqued image.")
# set common arguments
command.set_common(parser, threads=False)


def main():
    # parse arguments
    args = parser.parse_args()

    # call deoblique function
    img_deobliqued = affine.deoblique(nib.load(args.input_image))

    # save deobliqued image
    img_deobliqued.to_filename(args.output_image)
