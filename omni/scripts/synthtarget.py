"""
    Script to run SynthTarget
"""
import logging
import argparse
from omni import command
from .arguments import *

# create command line parser
parser = argparse.ArgumentParser(
    description='The SynthTarget algorithm. Solves the joint synthetic transformation/affine registration problem. '
                'See algorithm 1 of the Synth paper.',
    epilog='Author: Andrew Van, vanandrew@wustl.edu, 03/29/2021')
arg_target_image(parser, positional_arg=True, help="Target image to align to (should be [0,1] normalized).")
arg_interaction_model(parser, positional_arg=True)
arg_input_images(parser, positional_arg=True,)
arg_regression_weight_mask(parser, "-w")
arg_affine(parser, "-a", help="Initial affine transform to use for optimization.")
arg_output_prefix(parser, "-o", help="Output prefix for parameter solutions. Add .affine/.regress as suffixes.")
arg_regression_output(parser, "-r")
arg_synth_image(parser, "-b")
arg_synth_image_targetspace(parser, "-f")
arg_err_tol(parser, "-e")
arg_step_size(parser, "-d")
arg_max_iterations(parser, "-i")
arg_tone_curve(parser, "-c")
arg_tone_curve_mask(parser, "-u")
kernel_args = parser.add_mutually_exclusive_group()
arg_bandwidth(kernel_args, "-p")
arg_fixed_regress(parser)
arg_no_register(parser)
arg_output_intmat(parser)
# set common arguments
command.set_common(parser)


def main():
    # call parser
    args = parser.parse_args()
    logging.info("Arguments: %s", args)

    # set environment variables
    command.set_env(args)

    # call register function
    from omni import register
    register.register(
        target=args.target_image,
        interaction=args.interaction_model,
        source=args.input_images,
        regress_mask=args.regression_weight_mask,
        initial_affine=args.affine,
        output=args.output_prefix,
        regressed_output=args.regression_output,
        blurred_regressed_output=args.synth_image,
        aligned_output=args.synth_image_targetspace,
        err_tol=args.err_tol,
        step_size=args.step_size,
        max_iterations=args.max_iterations,
        tone_curve=args.tone_curve,
        tone_curve_mask=args.tone_curve_mask,
        bandwidth=args.bandwidth,
        fixed_regress=args.fixed_regress,
        no_register=args.no_register,
        output_intmat=args.output_intmat)
