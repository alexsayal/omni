#!/usr/bin/env python3
from pathlib import Path
from importlib import import_module
from genzshcomp import CompletionGenerator
from omni import scripts

THISDIR = Path(__file__).parent.resolve()

completions_dir = THISDIR / Path("zsh-completions")
completions_dir.mkdir(parents=True, exist_ok=True)

for script in Path(scripts.__path__[0]).iterdir():
    script = str(script.name)
    if not ("__init__.py" in script or "__pycache__" in script or "arguments.py" in script):
        try:
            script_basename = script.split(".")[0]
            script_name = "omni_" + script_basename
            print(script_basename)
            parser = import_module("omni.scripts.%s" % script_basename).parser
            generator = CompletionGenerator(script_name, parser)
            with open(completions_dir / ("_" + script_name), "w") as f:
                f.write(generator.get())
        except (AttributeError, TypeError):
            print("Could not generate completions for: %s" % script_basename)
